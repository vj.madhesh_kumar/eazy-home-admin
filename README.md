# Eazy Home Admin

## Description
### Abstract
Home automation, home control, smart or digital home are just different names for comfort, convenience, security and power saving. These systems are of increasing importance these days. Even though such systems are very expensive in general, they can also be very economical if one design and construct them for very specific needs. Some existing projects like Google Home and Amazon Alexa comes with data privacy or data theft allegations, however our project will aim to ensure that the personal data stays within the user control while providing the capabilities of home automation. Our project also features with Artificial Intelligence based security systems. 

Basically there are two modules in our solution, namely client and the server. The server will be able to run on your home PC or a cheap devices like Raspberry PI. The client is mobile app which is installed on an android smart phone. The app consist of user interface using which, user will be able to control home appliances via any Android Smartphone. On the server side there will be a dedicated Arduino board connected to the PC/Raspberry PI using USB cable to control the home appliances. The server-side module operates on Local Area Network with the required ports opened to connect with the client. So LAN or Wi-Fi is a must for the client-server communication. Also Port-Forwarding can be done to make your PC accept request with using your global IP address. 

In the server only an admin user can edit all the configuration and provision the appliances. The project also aims to provide a secure authentication for the home visitor by powering the server with an AI engine. This will facilitate the client app for face recognition and allow only the authorized person to entry the home. Along with the regular switching ON/OFF of the home appliances, the app can monitor the amount of minutes that each home appliances are turned ON. An simple AI is provided to predict your next electricity bill. Which may not be accurate but it will show you a way to utilize the appliances properly. There by you can save your electrical expenses.




This is a WIP Project all features may not be available( Features marked * is not present and planned to be implemented).
### Features:
- Simple to use.
- Encrypted request(SSL).
- Ease control of your home devices using android .*
- Face recognition implementation for intrusion detection system.*
- All data are stored locally. User Passwords are stored encrypted.
- Auto data summary of your power consuption of devices. *
- Control device behavior that uses weather data. Eg.: if sprinkler to be turned off when rain comes.It can't be turned on.*


[This will be updated soon]. All the configurations are stored in config.yaml file

## Help
### System Requirements:
#### Hardware:
- PC with 2 GB RAM (or)
- Raspberry PI 2/3/4
- A LAN Network
- Arudino UNO / MEGA

#### Software and Dependencies:
(Operating Systems supported)
- Windows 10 (Not yet tested)
- Linux (Debian and Arch based Distros)

This is tested well on Manjaro Linux(Arch based distro).It is recommended to use Linux for using the software. 

Needed Dependencies:
- Windows Dependencies
You need to have chocolately installed.

  Steps:
  1. Copy this to a powershell (Run it as administrator) :

    ```
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

    ```
  2.  Install Dependencies:
    ```choco install  python openssl```

- Linux Dependencies:
   - Manjaro/Arch Linux:
     ``` sudo pacman -S python openssl```
   - Debian/Ubuntu
     ```sudo apt install python openssl```

Python Dependencies (Common for Windows/Linux):

```pip install -r requirments.txt```

Note this can take time so be patient while installing.

## SLL file generation:
follow the command sequence and type the necessary information to generate a dummy ssl certificate for your home usage:
```
openssl req -new -key server.key -out server.csr
openssl rsa -in server.key.org -out server.key
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
```
## How to run:
run ```init.py```. Soon a shellscript and powershell script will be added for initial setup with a daemon Service.
configurations are stored in ```config.yaml```.

## Configuration
### Basic app configuration:
The configuration of app is stored in ``config.yaml``. It's conent will look like this: 
```

# Arduino Serial Port Configuration. Execute arduino.py to find the port.
# For Windows the serial port starts at COMXX for linux it is /dev/ttyACMX.
arduinoport: "/dev/ttyACM0"

# Connection configuration.
# Use HTTPS  true will enable encrypted request.
use-https:  True  
```
The configuration is done by editing the value.

To scan arduino ports run:
```python arduion.py``
### Adding home appliances (devices) and User:

#### Home appliances.

  The list of home appliances or devices connected to arduino is listed in ``devices.yaml``
  Use ``store.py`` to edit the list of home appliances or devices connected to arduino. you need to type ``python store.py -a`` and type the information of the appliance.

#### User:

  The list of users are listed in ``users.yaml``.
  The default user is admin with the password admin.
  Use ``store.py`` to edit the list of users that needs admin panel acess. you need to type ``python store.py -u`` and type the information of the appliance. Soon multiple home user feature will be implemented.
