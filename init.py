"""
This the main module that is used to run the application
"""
import yaml
from flask import Flask, redirect, url_for, render_template, request, session, jsonify
import os
import time
from passlib.hash import sha256_crypt as sha256
from arduino import arduino_connect, arduino_write
from store import *
import identify_face as rec
from datetime import datetime as dt

app = Flask(__name__, template_folder='template')
app.secret_key = os.urandom(12).hex()
try:
    with open('config.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    s = arduino_connect(data["arduinoport"])  # Arduino Device
    f.close()
except Exception as e:
    print("Could not connect to the Arduino device.")
    quit()


@app.route("/")
def index():
    # session["username"]="admin"
    # Un-comment the above line only for debugging purpose
    if "username" in session:  # Verify login
        try:
            
            x = tuple(list_devices_name_detailed())
            return render_template("index.html", x=x)
        except Exception as e:
            return render_template("index.html", error=e)
    else:
        return redirect(url_for("login"))


# setting / changing admin password:
# This will be changed in future update for now it is commented out.
'''
@app.route("/change-admin-pass", methods=['GET', 'POS'])
def change_password():
    if "username" in session:
        if session["username"] == "admin":
            if request.method == "POST":
                p1 = request.form["password1"]
                p2 = request.form["password2"]
                if p1 == p2:
                    try:
                        c, conn = connect_db()
                        user = 'admin'
                        hashx = sha256.encrypt(p1)
                        c.execute("insert into users (name,password) values(%s,%s)", (user, hashx))
                        conn.commit()
                        clean(c, conn)
                    except Exception as e:
                        return render_template("change-admin-pass.html", error=e)
                else:
                    return render_template("change-admin-pass.html", error="password did not match")
    else:
        return redirect(url_for("login"))


# This is not in use currently
@app.route("/register-users", methods=["POST", "GET"])
def register():
    user = str(request.form["username"])
    if request.method == "POST":
        paswd = request.form["password"]
        hashedpassword = sha256.encrypt(paswd)
        c, conn = connect_db()
        var = '\'' + user + '\''
        command = "select * from users where name=" + var
        c.execute(command)
        x = c.fetchall()
        print(command)
        c.execute(command, var)
        if int(len(x)) > 0:
            return render_template("register-users.html", error="username already taken")
        else:
            try:
                c.execute("insert into users (name,password) values (%s,%s)", (user, hashedpassword))
                conn.commit()
                c.close()
                conn.close()
                gc.collect()
                session['user'] = user
                return redirect(url_for("index"))
            except Exception as e:
                return str(e)
    else:
        if "user" in session:
            return redirect(url_for("index"))
        else:
            return render_template("register-users.html")
'''

# Settings page
@app.route("/settings", methods=['GET', 'POS'])
def settings_page():
    if "username" not in session:
        redirect(url_for("login"))
    else:
        if request.method == "GET":
            return render_template("settings.html")
        else:
            print("A post request was made to settings")


# login page
@app.route("/login", methods=["POST", "GET"])
def login():
    if "username" in session:  # check if user is in the session
        return redirect(url_for("index", usr=session["username"]))
    else:
        try:
            if request.method == "POST":
                user = request.form['username']
                password = request.form['password']
                x = verify_user(user,password)
                if x is None:
                    return render_template("login.html", error="User name not found")
                elif x == True:
                    session["username"] = user
                    return redirect(url_for("index"))
                else:
                    return render_template("login.html", error="Wrong login Credentials!")
            else:
                return render_template("login.html")
        except Exception as e:
            return render_template("login.html", error=e)


@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("login"))


# Main Code that consist of Devices command
@app.route("/device/<devname>/<status>")
def device_toggle(devname, status):
    try:
        x = list_devices_name()
        if devname not in x:
            return jsonify({'Error': 'Device not found'})
        else:
            stuff, flag = arduino_write(s, devname + ":" + status)

            if flag:
                change_device_status(devname, status)
                if stuff.find("error") or stuff.find("Error"):
                    return str(stuff)

    except Exception as e:
        return str(e)


@app.route("/api/device/<devname>/<status>")
def api_device_toggle(devname, status):
    try:
        x = list_devices_name()
        if devname not in x:
            return jsonify({'Error': 'Device not found'})
        else:
            stuff, flag = arduino_write(s, devname + ":" + status)
            change_device_status(devname, status)
            return api_status(stuff)
    except Exception as e:
        json_dict = {'error': str(e)}
        return jsonify(json_dict)


# Translates the device table into JSON File
@app.route("/api/getdevicestatus")
def api_status(msg="List of all Devices"):
    try:
        devices = get_all_devices_status()
        json_dict = dict()
        json_dict['Info'] = msg
        # Convert devices tuple to JSON
        for i in devices:
            for j in i:
                json_dict[i[0]] = i[1]
        
        return jsonify(json_dict)
    except Exception as e:
        return jsonify(e)


@app.route("/usage_summary")
def usage_summary():
    return(render_template('usage_summary.html',time_stamp= dt.now()))


# Error Handling Methods
@app.errorhandler(404)
def not_found(e):
    return "Webpage not found"


@app.errorhandler(403)
def permission_denied(e):
    return "Access Denied!"


@app.errorhandler(400)
def bad_request(e):
    return str(e)


@app.errorhandler(500)
def internal_error(e):
    return "Internal error: " + str(e)


@app.errorhandler(503)
def int_error(e):
    return "Internal error: " + str(e)


def main():
    time.sleep(3)
    print("Starting the app...")
    rec.train_face_cache()
    device_list = list_devices_name()
    for device in device_list:
        stuff, flag = arduino_write(s, device + ":" + "off")
    change_all_device_status("off")
    # HTTPS requires to run the script as root or administrator.
    # app.run(host='0.0.0.0',debug=False,port=443,ssl_context=('server.crt','server.key'),threaded=True)
    app.run(host='0.0.0.0', debug=False, port=80, threaded=True)  # HTTP


if __name__ == "__main__":
    main()
