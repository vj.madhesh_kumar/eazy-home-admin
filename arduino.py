"""
This is used for communication with arduino
"""

import sys
import glob
import serial
import yaml
import time

data = None
with open('config.yaml') as f:
    data = yaml.load(f, Loader=yaml.FullLoader)

    
def list_serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')
    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def arduino_write(s ,data):
    try:
        s.write(data.encode())
        x = s.readline().decode('utf-8')
        print(x)
        return(x,True)
    except Exception as e:
        print(e)
        return(e,False)


def arduino_connect(port):
    s = serial.Serial(port,baudrate=9600,timeout=2)
    time.sleep(3)
    return s
    

if __name__ == '__main__':
    if len(sys.argv)>=2:
        if sys.argv[1] =="test":
            arduino = arduino_connect(data["arduinoport"])
            arduino_write(arduino,"test")
        else:
            s = list_serial_ports()
            print(s)