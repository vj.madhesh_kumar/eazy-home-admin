#this is very simple shell script to use OpenSSl
# run it on manjaro/any arch based distro
sudo pacman -S openssl python mariadb -y
openssl genrsa -des3 -out server.key 1024
openssl req -new -key server.key -out server.csr
cp server.key server.key.org
openssl rsa -in server.key.org -out server.key
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
