import yaml
from passlib.hash import sha256_crypt as sha256
from getpass import getpass
import sys
from datetime import datetime as dt


def add_devices(name, type_dev='passive'):
    with open(r'devices.yaml','r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    try:
        d = data[name]
        print("Device already Exist: " + str(d))
        f.close()
    except:
        try:
            count = len(data)
        except Exception as e:
            count = 0
        f.close()
        dict_devices = dict()
        dict_devices[name] = dict()
        dict_devices[name]['type'] = type_dev
        dict_devices[name]['added_on'] = dt.now()
        dict_devices[name]['updated_on'] = dt.now()
        dict_devices[name]['status'] = 'off'
        dict_devices[name]['id'] = count +1
        dict_devices[name]['usage'] = 0

        with open(r'devices.yaml', 'a+') as f:
            yaml.dump(dict_devices, f) 


def create_user(username , password,c=False):
    with open('user.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    f.close()
    try: 
        data[username]
        f.close()
        if c==True:
            choice = input('The username is already taken. Do you want to change the password (y/n): ')
            if choice == 'y':
                with open('user.yaml') as f:
                    data = yaml.load(f, Loader=yaml.FullLoader)
                hashedpassword = sha256.hash(password)
                data[username]['password']= hashedpassword
                with open('user.yaml', 'w') as f:
                        yaml.dump(data, f)
                return 'Password Changed.'
        elif c==False:
            return 'Username Exists.'
    except:
        hashedpassword = sha256.encrypt(password)
        dict_p = dict()
        dict_p[username]= dict()
        dict_p[username]['settings'] = 'default'
        dict_p[username] ['password'] = hashedpassword
        with open(r'user.yaml', 'a+') as f:
                yaml.dump(dict_p, f)
        f.close()


def verify_user(username, password):
    with open('user.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    userdata = data[username]
    if userdata:
        res = sha256.verify(password,userdata['password'])
        if res:
            print("Password verified as correct ")
            f.close()
            return True
        else:
            print("Wrong password")
            f.close()
            return False
    else:
        f.close()
        return None
    

    
def list_devices_name():
    # function to list all devices
    with open(r'devices.yaml','r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    f.close()
    return tuple(data.keys())


def change_device_status(device, status):
    # function to change device status
    l_devices = list_devices_name()
    if device in l_devices:
        with open('devices.yaml') as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        data[device]['status']= status
        if status == 'off':
            diff = dt.now() - data[device]['updated_on']
            days, sec = diff.days , diff.seconds

            hour = (diff.days *24)+ (sec/60)/60
            data[device]['usage'] += hour

        data[device]['updated_on'] = dt.now()
        with open('devices.yaml', 'w') as f:
            yaml.dump(data, f)
        return 'Device status changed.'
    else:
        return "Device not found in the file. Please update the file. Else the data will be inconsistent."


def get_all_devices_status():
    # function to get all devices status
    l_devices = list_devices_name()
    with open(r'devices.yaml','r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    mylist = list()
    for i in l_devices:
        mylist.append((i, data[i]['status']))
    return tuple(mylist)


def list_devices_name_detailed():
    # function to list all devices with Details
    with open(r'devices.yaml','r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    keys = list(data.keys())
    mylist = []    
    for i  in keys:
        mylist.append((data[i]['id'],i , data[i]['updated_on'], data[i]['status'], data[i]['type']))
    return mylist


def change_all_device_status(status):
    # function to change all device status
    l_devices = list_devices_name()
    for i  in l_devices:
        change_device_status(i, status)


def get_all_devices_usage():
    # function to get all devices status
    l_devices = list_devices_name()
    with open(r'devices.yaml','r') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    mylist = list()
    for i in l_devices:
        mylist.append((i, data[i]['usage']))
    return tuple(mylist)


def graph():
    usage = get_all_devices_usage()
    xaxis,yaxis = list(),list()
    for i in range(usage):
        xaxis.append(usage[i][0])
        yaxis.append(usage[i][1])
        plt.bar(xaxis,yaxis)
    plt.title('current total usage')
    plt.xlabel('devices')
    plt.ylabel('usage')
# plt.show()
    plt.savefig('./static/graphs/barchart_summary.png')


def clear_all_device_usage():
    with open('devices.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    l_devices = list_devices_name()
    date = dt.now()
    os.rename('./static/graphs/barchart_summary.png','./static/graphs/barchart_summary_'+ str(date)+'.png') 
    for i in l_devices:
        data[i]['usage']=0
    with open('devices.yaml', 'w') as f:
        yaml.dump(data, f)

    
if __name__ == '__main__':
    # Command Line Args to help users configure the storage backend
    if len(sys.argv) > 1:
        if sys.argv[1] == 'verify' or sys.argv[1] == '-v':
            x = input('Enter the username: ')
            y = getpass('Enter the password: ')
            verify_user(x, y)
        elif sys.argv[1] == 'user' or sys.argv[1] =='-u':
             x = input('Enter the username: ')
             y = getpass('Enter the password: ')
             create_user(x, y, True)
        elif sys.argv[1] == 'device' or sys.argv[1] =='-a':
            # Add devices
            name = input('Enter the name: ')
            type_dev = input('Enter the type of device (Default : passive): ')
            add_devices(name,type_dev)
        elif sys.argv[1] == '-ld':
            # List all devices
            devices = list_devices_name_detailed()
            # print(devices)
            t_device = tuple(devices)
            print(t_device)
        elif sys.argv[1] == '-cs':
            # Change device status
            x = input('Enter the device name: ')
            y = input('Enter the status: ')
            msg = change_device_status(x ,y)
        elif sys.argv[1] == '-gs':
            print(get_all_devices_status())
        else:
            print("-v verify : Verify Username and password")
            print("-u user: add new user (Password is changed if the user account already exists.)")
            print("-a device: add new device")
            print("-ld: list all devices")
            print("-cs: change device status")
            print("-gs: get all devices status")
          
    else:
        print("-v verify : Verify Username and password")
        print("-u user: add new user (Password is changed if the user account already exists.)")
        print("-a device: add new device")
        print("-ld: list all devices")
        print("-cs: change device status")
        print("-gs: get all devices status")
          
